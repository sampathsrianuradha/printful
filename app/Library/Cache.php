<?php

namespace App\Library; 

 /**
 * Class Cache
 * 
 * Cashing 
 * 
 */
class Cache implements CacheInterface
{
    protected $cachePath = '/tmp';
    protected $duration = 300;
    protected $cachePathChmod = 0775;
    protected $entryFileChmod = 0664;
    /**
     * @param string $cachePath
     * @param int    $cachePathChmod
     * @throws \Exception
     */
    public function __construct()
    {
        $this->_initializeCachePath();
    }
    /**
     * @param integer $cachePathChmod
     */
    public function setCachePathChmod($cachePathChmod)
    {
        if (is_numeric($cachePathChmod))
        {
            $this->cachePathChmod = $cachePathChmod;
        }
    }
    /**
     * @param string $cachePath
     */
    public function setCachePath($cachePath)
    {
        $this->cachePath = rtrim($cachePath, '/') . '/';
        $this->_initializeCachePath();
    }
    /**
     * set
     *
     * @param string $key
     * @param mixed  $value
     * @param int    $duration  
     *
     * @return void
     */
    public function set(string $key, $value, int $duration = 300)
    {
        $entryFilename = $this->getEntryFilenameByKey($key);
        $cacheData = array(
            'value' => $value,
            'expires' => time() + (($duration !== NULL) ? $duration : $this->duration)
        );
        file_put_contents($entryFilename, serialize($cacheData));
        chmod($entryFilename, $this->entryFileChmod);
    }
    /**
     * get
     *
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        $filename = $this->getEntryFilenameByKey($key);
        if (file_exists($filename))
        {
            $cacheData = unserialize(file_get_contents($filename));
            if ($cacheData['expires'] >= time())
            {
                return $cacheData['value'];
            }
            unlink($filename);
        }
        return NULL;
    }
    /**
     * @return string
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }
    /**
     * @param string $key
     *
     * @return string
     */
    public function getEntryFilenameByKey($key)
    {
        return $this->getCachePath() . '/' . $key . '.dat';
    }
    /**
     * @param $key
     * @return void
     */
    public function delete($key)
    {
        $entryFilename = $this->getEntryFilenameByKey($key);
        if (file_exists($entryFilename))
        {
            unlink($entryFilename);
            return true;
        }
        return false;
    }
    /**
     * initializeCacheDir
     */
    protected function _initializeCachePath()
    {
        $cachePath = $this->getCachePath();
        if (!is_writable(dirname($cachePath)))
        {
            throw new \Exception('cannot write in "' . dirname($this->getCachePath()) . '": Permission denied');
        }
        if (!is_dir($cachePath))
        {
            mkdir($cachePath, $this->cachePathChmod, TRUE);
        }
    }
}